const apiHost = 'http://localhost:8080'

const apiRequest = async (endpoint, method = 'GET', data = null) => {
    const refNum = (new Date()).getTime();
    const opts = {
        method,
        headers: {
            'Content-Type': 'application/json',
            'Reference-Number': refNum,
        },
        // mode: 'cors',
    }
    if (data) {
        opts.body = JSON.stringify(data)
    }

    const resp = await fetch(`${apiHost}${endpoint}`, opts);
    if (resp.status >= 400) {
        const data = await resp.json();
        throw Error(data.message || 'something went wrong')
    }

    return resp.json();
}

const getFormdata = formRef => {
    const data = {}
    const fd = new FormData(formRef)
    for (const [name, value] of fd) {
        data[name] = value
    }

    if (Boolean(data.is_active)) {
        data.is_active = true
    }

    return data
}